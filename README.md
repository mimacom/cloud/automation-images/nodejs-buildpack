# Node.js Buildpack Docker Image

This docker image can be used to build the [Node.js buildpack](https://github.com/cloudfoundry/nodejs-buildpack).
The image contains Go and [buildpack-packager](https://github.com/cloudfoundry/libbuildpack/tree/master/packager/buildpack-packager)