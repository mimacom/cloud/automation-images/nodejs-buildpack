FROM golang:1.11

RUN go get github.com/cloudfoundry/libbuildpack/packager/buildpack-packager
RUN go install github.com/cloudfoundry/libbuildpack/packager/buildpack-packager